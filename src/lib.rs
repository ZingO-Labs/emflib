/// Extract, validate, and parse Encrypted Memo Field (EMF) contents.
///
/// The EMF is a 512 byte blob packed inside shielded transactions.  As such
/// it has the privacy/security properties provided by the zcash encryption
/// protocol.
///
/// This library provides the following functionality:
///
/// Invoke a zcash-cli rpc command to produce a string representing the
/// decrypted contents of a shielded transaction (NOTE: different levels of
/// privilege are possible).
///
/// Convert the produced string into a Rust type using the serge-json
///
///
/// Validate that such an extracted JSON is a valid ZcCliTransaction
/// instance.
///
///
/// Notes from:  https://docs.serde.rs/serde/de/trait.Visitor.html#method.visit_str
/// That's probably what we want.
pub fn register_app() {
    unimplemented!();
    // 0 sign a commit with the app gpg key
    // 1 send a message to the registry address containing:
    //    * signature
    //    * URL to commit
    //    * the verifying key
}
use serde_json::{map::Map, Value};
pub trait Response {}
pub trait MemoContainer {
    type Responder: Response;
    fn access_memos(memos_source: &Self::Responder) -> Result<Vec<Vec<u8>>, MemoContainerError>;
    fn access_memo(transaction: &Map<String, Value>) -> Result<Vec<u8>, MemoContainerError>;
}
#[derive(Debug)]
pub enum MemoContainerError {
    MemoAccess,
}
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

use serde_json::Error;

#[derive(Debug, Serialize, Deserialize)]
pub struct Amount {
    value: u64,
} // 0 <= value <= 21000000000000
/// I am using this:
/// https://zcash-rpc.github.io/z_listreceivedbyaddress.html
/// as my template for this:

#[derive(Debug, Serialize, Deserialize)]
pub struct ZcTransactions<'a> {
    #[serde(borrow)]
    transactions: Vec<ZcTransaction<'a>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ZcTransaction<'a> {
    txid: String,
    amount: Amount,
    #[serde(borrow)]
    memo: MemoField<'a>,
    outindex: u64,
    change: bool,
}

use std::str;
#[derive(Debug, Clone)]
pub enum DeserializeMemoFieldError {
    BadUTF8(str::Utf8Error),
    NotValidError,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct RegisteredApp<'a> {
    pubkey_fingerprint: &'a [u8],
    content_payload: &'a [u8],
}

impl<'a> RegisteredApp<'a> {
    fn new(pubkey_fingerprint: &'a [u8], content_payload: &'a [u8]) -> RegisteredApp<'a> {
        RegisteredApp {
            pubkey_fingerprint,
            content_payload,
        }
    }
    //Maybe this will be migrated into a Trait
    fn validate_app(&self) -> bool {
        // 0 Extract the first 5 bytes
        // 1 lookup a pubkey using the 5 bytes as a dict key
        // 2 verify(-ing) that the acquired pubkey has the 5 byte finger print
        // 3
        unimplemented!()
    }
    fn retrieve_pubkey(&self) {
        unimplemented!()
    }
    fn retrieve_app(&self) {
        unimplemented!()
    }
}

use std::borrow::Cow;
#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum MemoField<'a> {
    #[serde(borrow)]
    UTF8(Cow<'a, str>),
    #[serde(borrow)]
    Registration(RegisteredApp<'a>),
}

/// https://github.com/zcash/zcash/issues/1849
/// https://github.com/zcash/zips/pull/105
impl<'a> MemoField<'a> {
    pub fn new(serialbytes: &'a [u8]) -> Result<MemoField, DeserializeMemoFieldError> {
        assert_eq!(serialbytes.len(), 512);
        match &serialbytes[0] {
            n if n <= &0xf4 => match str::from_utf8(&serialbytes[..]) {
                Ok(contents) => Ok(MemoField::UTF8(Cow::Borrowed(contents))),
                Err(utf8_error) => Err(DeserializeMemoFieldError::BadUTF8(utf8_error)),
            },
            n if n == &0xf5 => Ok(MemoField::Registration(RegisteredApp::new(
                &serialbytes[1..6],
                &serialbytes[6..512],
            ))),
            _ => Err(DeserializeMemoFieldError::NotValidError),
        }
    }
}

pub fn deserialize_transactions(serial_txs: &str) -> Result<Vec<ZcTransaction>, Error> {
    println!("{:?}", serial_txs);
    //let sj: ZcTransactions = serde_json::from_str(serial_txs).expect(
    //    "Failed to deserialize using from_str");
    let sj: Vec<ZcTransaction> =
        serde_json::from_str(serial_txs).expect("Failed to deserialize using from_str");
    println!("{:?}", sj);
    Ok(sj)
}

mod testdata;
#[cfg(test)]
mod tests {
    //mod data;
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
        //        assert_eq!(deserialize_transactions(
        //            "ztestsapling\
        //            1fpf39y8htnhepvhzrw5ht5ma69gkwnnv9vnz6\
        //            yxeaqmf7u6q6qawmfedtpmpjgzhq5zmvfdx533"
        //        ), ());
    }

    #[test]
    fn test_memofield_constructor_on_zeros() {
        assert_eq!(
            MemoField::new(&[0u8; 512]).unwrap(),
            MemoField::UTF8(Cow::Borrowed(testdata::utf8_cases::ALL_ZEROES))
        )
    }
    #[test]
    fn test_memofield_constructor_with_invalid_utf8() {
        if let Err(DeserializeMemoFieldError::BadUTF8(string_err)) =
            MemoField::new(&testdata::utf8_cases::INVALID_UTF8)
        {
            assert_eq!(38, string_err.valid_up_to())
        }
    }

    #[test]
    fn test_memofield_construction_of_app() {
        assert_eq!(
            MemoField::new(&testdata::app_registered_data::TWOFORTYFIVEZEROES).unwrap(),
            MemoField::Registration(RegisteredApp::new(&[0x00; 5], &[0x00; 506]))
        )
    }
}
