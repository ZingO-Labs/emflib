extern crate emflib;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);
    let zaddress = &args[1];
    let x = emflib::call_z_listreceivedbyaddress(&zaddress);
    //println!("{:?}", x.stdout.expect("Failed to open stdout, for echo"));
    //println!("{:?}", String::from_utf8(x.stdout).unwrap().trim());
    match emflib::deserialize_transactions(&String::from_utf8(x.stdout).unwrap()) {
        Ok(_r) => println!("Yay!"),
        Err(_e) => panic!(),
    }
    //println!("{:?}", emflib::MemoField::new());
}
